package synchronize;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class HTTPSCertifications {

	public static void main(String[] args) {

		// General Chrome profile
		DesiredCapabilities dc = DesiredCapabilities.chrome();
//		dc.acceptInsecureCerts();
		dc.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
	
		// Belongs to your local browser
		ChromeOptions c = new ChromeOptions();
		c.merge(dc);
		System.setProperty("webdriver.chrome.driver",
				"C://Users//91974//Downloads//chromedriver_win32 (1)//chromedriver.exe");
		WebDriver driver = new ChromeDriver(c);
		driver.get("https://lucky.toprankers.com");
		driver.manage().window().maximize();
		driver.close();
	}

}
