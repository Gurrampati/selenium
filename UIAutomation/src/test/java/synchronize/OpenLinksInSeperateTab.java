package synchronize;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class OpenLinksInSeperateTab {

	public static void main(String[] args) throws Exception {

		System.setProperty("webdriver.chrome.driver", "C://Users//91974//Downloads//chromedriver_win32 (1)//chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.rahulshettyacademy.com/AutomationPractice/");
		System.out.println(driver.findElements(By.tagName("a")).size());
		
		//Limiting WebDriver Scope
		WebElement footer = driver.findElement(By.id("gf-BIG"));
		System.out.println(footer.findElements(By.tagName("a")).size());
		
		WebElement footer_column = footer.findElement(By.xpath("//table/tbody/tr/td[1]/ul"));
		System.out.println(footer_column.findElements(By.tagName("a")).size());
		
		//Click on each link in the column and check if the pages are opening
		for(int i=1; i<footer_column.findElements(By.tagName("a")).size(); i++)
		{
			String clickOnLinks = Keys.chord(Keys.CONTROL, Keys.ENTER);
			footer_column.findElements(By.tagName("a")).get(i).sendKeys(clickOnLinks);
			Thread.sleep(5000L);
		}
		
		//Open tabs and get the title of the page
		
		Set<String> tab = driver.getWindowHandles();
		Iterator<String> it = tab.iterator();
		while(it.hasNext())
		{
			driver.switchTo().window(it.next());
			System.out.println(driver.getTitle());
		}
		
		driver.close();
		
	}
	

}
