package Basics;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class XpathWithParent_ChildTraverseRelationship {

	public static void main(String[] args) {

		// Setting system properties of ChromeDriver
		System.setProperty("webdriver.chrome.driver",
				"C://Users//91974//Downloads//chromedriver_win32 (1)//chromedriver.exe");

		// Creating an object of ChromeDriver
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();

		driver.get("https://www.google.com/");
		// Traverse from parent to child
		driver.findElement(By.xpath("//div[@class='gb_h gb_i']/a"));
		System.out.println("Clicked on google images");
		driver.close();

	}

}
