package synchronize;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Synchroniz_ImplicitlyWait {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "C://Users//91974//Downloads//chromedriver_win32 (1)//chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		driver.get("https://alaskatrips.poweredbygps.com/");
		
		//Source
		WebElement source = driver.findElement(By.xpath("//input[@id='package-origin-hp-package']"));
		source.click();
		source.sendKeys("newyork");
		source.sendKeys(Keys.TAB);
		source.sendKeys(Keys.ENTER);
		
		//Destination
		WebElement destination = driver.findElement(By.id("package-destination-hp-package"));
		destination.click();
		destination.sendKeys("Zambia");
		destination.sendKeys(Keys.TAB);
		destination.sendKeys(Keys.ENTER);
		
		//StartDate
		driver.findElement(By.id("package-departing-hp-package")).click();
		
		

	}

}
