package Basics;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class HandlingRadioButtonsDynamically {

	public static void main(String[] args) {

		// Setting system properties of chromeDriver
		System.setProperty("webdriver.chrome.driver",
				"C://Users//91974//Downloads//chromedriver_win32 (1)//chromedriver.exe");

		// Create an object of chromeDriver
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://www.echoecho.com/htmlforms10.htm");

		// Count of radio buttons
		int count = driver.findElements(By.xpath("//input[@name='group1']")).size();
		for (int i = 0; i < count; i++) {
			String text = driver.findElements(By.xpath("//input[@name='group1']")).get(i).getAttribute("value");
			if (text.equals("Cheese")) {
				driver.findElements(By.xpath("//input[@name='group1']")).get(i).click();
			}
		}

		driver.close();

	}

}
