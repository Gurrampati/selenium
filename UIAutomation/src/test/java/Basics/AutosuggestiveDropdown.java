package Basics;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AutosuggestiveDropdown {

	public static void main(String[] args) throws Exception {
		//Setting system properties of ChromeDriver
		System.setProperty("webdriver.chrome.driver","C://Users//91974//Downloads//chromedriver_win32 (1)//chromedriver.exe");
		
		//Create an object of chromeDriver
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.google.com/");
		
		WebElement source = driver.findElement(By.xpath("//input[@name='q']"));
		source.sendKeys("Zohomail");
		source.sendKeys(Keys.DOWN);
		source.sendKeys(Keys.ENTER);
		driver.close();

	}

}
