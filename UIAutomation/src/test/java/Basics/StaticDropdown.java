package Basics;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class StaticDropdown {

	public static void main(String[] args) throws Exception {
		
		//Setting system properties of ChromeDriver
		System.setProperty("webdriver.chrome.driver","C://Users//91974//Downloads//chromedriver_win32 (1)//chromedriver.exe");
		
		//Create an object of chromeDriver
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.spicejet.com/");
		
		//Static Dropdown
		new Select(driver.findElement(By.id("ctl00_mainContent_DropDownListCurrency"))).selectByValue("USD");
		System.out.println("USD is selected");
		driver.close();

	}

}
