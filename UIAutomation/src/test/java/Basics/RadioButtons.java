package Basics;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class RadioButtons {

	public static void main(String[] args) {
		
		//Setting system properties of chromeDriver
		System.setProperty("webdriver.chrome.driver", "C://Users//91974//Downloads//chromedriver_win32 (1)//chromedriver.exe");
		
		//Create an object of chromeDriver
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://www.echoecho.com/htmlforms10.htm");
		
		//Select Radio buttons
		driver.findElement(By.xpath("//input[@value='Milk']")).click();
		
		//Count of radio buttons
		System.out.println(driver.findElements(By.xpath("//input[@name='group1']")).size());
		driver.close();

	}

}
