package Basics;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class DynamicDropdown_Index {

	public static void main(String[] args) throws Exception {
		
		//Setting system properties of ChromeDriver
		System.setProperty("webdriver.chrome.driver","C://Users//91974//Downloads//chromedriver_win32 (1)//chromedriver.exe");
		
		//Create an object of chromeDriver
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.spicejet.com/");
		
		//Dynamic Dropdown
		driver.findElement(By.id("ctl00_mainContent_ddl_originStation1_CTXT")).click();
		
		//Departure City
		driver.findElement(By.xpath("//a[@value='BLR']")).click();
		Thread.sleep(2000);
		
		//Arrival City
		driver.findElement(By.xpath("(//a[@value='MAA'])[2]")).click();
		driver.close();

	}

}
