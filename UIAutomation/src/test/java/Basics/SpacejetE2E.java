package Basics;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class SpacejetE2E {

	public static void main(String[] args) throws Exception {
		
		//Setting system properties of chromeDriver
		System.setProperty("webdriver.chrome.driver", "C://Users//91974//Downloads//chromedriver_win32 (1)//chromedriver.exe");
		
		//Create an object of chromeDriver
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.spicejet.com/");
		
		//Dropdown
		driver.findElement(By.id("ctl00_mainContent_ddl_originStation1_CTXT")).click();
		
		//Source
		driver.findElement(By.xpath("//div[@id='glsctl00_mainContent_ddl_originStation1_CTNR'] //a[@value='BLR']")).click();
		Thread.sleep(2000);
		
		//Destination
		driver.findElement(By.xpath("//div[@id='glsctl00_mainContent_ddl_destinationStation1_CTNR'] //a[@value='MAA']")).click();
		
		//Current Date
		driver.findElement(By.cssSelector(".ui-state-default.ui-state-highlight.ui-state-active")).click();
		
		//Select oneway trip
		driver.findElement(By.id("ctl00_mainContent_rbtnl_Trip_0")).click();
		
		//Validate whether the enddate is active or not
		if(driver.findElement(By.id("Div1")).getAttribute("style").contains("0.5"))
		{
			System.out.println("End date is disabled");
			Assert.assertTrue(true);
		}
		else
		{
			Assert.assertFalse(false);
		}
		
		//Select passengers
		driver.findElement(By.id("divpaxinfo")).click();
		Thread.sleep(1000);
		new Select(driver.findElement(By.id("ctl00_mainContent_ddl_Adult"))).selectByValue("6");
		
		//Search
		driver.findElement(By.id("ctl00_mainContent_btn_FindFlights")).click();
		driver.close();
	
	}

}
