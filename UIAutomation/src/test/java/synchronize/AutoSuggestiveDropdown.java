package synchronize;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class AutoSuggestiveDropdown {

	public static void main(String[] args) {

		// Setting system properties of chromeDriver
		System.setProperty("webdriver.chrome.driver",
				"C://Users//91974//Downloads//chromedriver_win32 (1)//chromedriver.exe");

		// Create an object of chromeDriver
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.amazon.com/");
		
		driver.findElement(By.id("twotabsearchtextbox")).sendKeys("Iphone Black");
		driver.findElement(By.id("twotabsearchtextbox")).sendKeys(Keys.DOWN);
//		driver.findElement(By.id("twotabsearchtextbox")).sendKeys(Keys.DOWN);
//		driver.findElement(By.id("twotabsearchtextbox")).click();
		
		System.out.println(driver.findElement(By.id("twotabsearchtextbox")).getText());
		
		//JavaScriptExecutor - To find the hidden elements
		JavascriptExecutor js = (JavascriptExecutor)driver;
		String script = "return document.getElementById(\"twotabsearchtextbox\").value;";
		String text = (String) js.executeScript(script);
		System.out.println(text);
	}

}
